"""Test cases for misc module."""
import unittest
from unittest import mock

from bs4 import BeautifulSoup as BS
from ruamel.yaml.representer import RoundTripRepresenter
from ruamel.yaml.representer import ScalarNode

from upt import const
from upt import misc


class TestMisc(unittest.TestCase):
    """Test cases for misc module."""

    def setUp(self) -> None:
        self.test_recipe = """<recipe status="Aborted">
            <task name="a1" status="Aborted">
                <param name="KILLTIMEOVERRIDE" value= "10" />
            </task>
            <task name="/distribution/command" status="Cancelled">
                <param name="KILLTIMEOVERRIDE" value= "20" />
            </task>
            <task name="a3" status="Cancelled" >
                <param name="DONTCARE" value= "20" />
                <fetch />
            </task>
            <task name="a4" status="Cancelled" >
                <param name="KILLTIMEOVERRIDE" value="INV" />
            </task>
         </recipe>"""
        self.cancelled_recipe = """<recipe status="Cancelled" />"""
        self.no_status_recipe = """<recipe />"""
        self.invalid_status_recipe = """<recipe status="Blah"/>"""

    def test_compute_recipe_duration(self):
        """Ensure compute_recipe_duration works."""
        expected_duration = misc.compute_recipe_duration(BS(self.test_recipe,
                                                            'xml'))
        # total KILLTIMEOVERRIDE 's of tasks is 30
        self.assertEqual(expected_duration, 30)

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_fixup_or_delete_tasks_without_fetch(self):
        """Ensure fixup_or_delete_tasks_without_fetch works."""
        soup = BS(self.test_recipe, 'xml')
        copy_task = BS(self.test_recipe, 'xml').findAll('task')[-3]

        misc.fixup_or_delete_tasks_without_fetch(soup)
        # Task a1 is removed, task /distribution/command is kept and url
        # is inserted. Task a3 is kept as well.
        left_tasks = soup.findAll('task')
        self.assertEqual(len(left_tasks), 2)

        left_task1 = soup.findAll('task')[0]
        self.assertEqual(left_task1['name'], copy_task['name'])
        self.assertEqual(left_task1.find('fetch')['url'],
                         const.BKR_CORE_TASKS_URL.format(task='command'))

        left_task2 = soup.findAll('task')[1]
        self.assertEqual(left_task2['name'], 'a3')

    def test_recipe_not_provisioned(self):
        """Ensure recipe_not_provisioned works."""
        recipe1 = BS(self.test_recipe, 'xml').find('recipe')
        recipe2 = BS(self.cancelled_recipe, 'xml').find('recipe')
        recipe3 = BS(self.no_status_recipe, 'xml').find('recipe')
        recipe4 = BS(self.invalid_status_recipe, 'xml').find('recipe')

        recipe1_status = misc.recipe_not_provisioned(recipe1)
        recipe2_status = misc.recipe_not_provisioned(recipe2)
        recipe3_status = misc.recipe_not_provisioned(recipe3)
        recipe4_status = misc.recipe_not_provisioned(recipe4)

        self.assertEqual(recipe1_status, 'Aborted')
        self.assertEqual(recipe2_status, 'Cancelled')
        self.assertEqual(recipe3_status, False)
        self.assertEqual(recipe4_status, False)

    def test_reservesys_task_problem(self):
        """Ensure reservesys_task_problem works."""
        tasks = BS(self.test_recipe, 'xml').findAll('task')
        self.assertEqual(misc.reservesys_task_problem([tasks[0]]), 'Aborted')
        self.assertEqual(misc.reservesys_task_problem([tasks[1]]), 'Cancelled')
        self.assertEqual(misc.reservesys_task_problem(
            BS('<task name="a3" status="None" result="Pass"/>', 'xml')), False)
        self.assertEqual(misc.reservesys_task_problem(
            BS('<task name="a4" status="None" result="Fail"/>', 'xml')), 'Fail')

    def test_recipe_installing_or_waiting(self):
        """Ensure recipe_installing_or_waiting works."""
        recipe = BS(self.test_recipe, 'xml').find('recipe')
        self.assertFalse(misc.recipe_installing_or_waiting(recipe))

        recipe_inst = BS('<recipe status="Installing" />', 'xml').find('recipe')
        self.assertTrue(misc.recipe_installing_or_waiting(recipe_inst))

        recipe_inst = BS('<recipe status="Waiting" />', 'xml').find('recipe')
        self.assertTrue(misc.recipe_installing_or_waiting(recipe_inst))

    def test_get_whiteboard(self):
        """Ensure get_whiteboard works."""
        template = '<whiteboard>cki@gitlab{suf}</whiteboard>'

        result = misc.get_whiteboard(template.format(suf=''), 1, True)
        self.assertEqual(template.format(suf=' #01 PREPROVISIONED'), result)
        result = misc.get_whiteboard(template.format(suf=''), 1, False)
        self.assertEqual(template.format(suf=' #01'), result)

    def test_merge_retcodes(self):
        """Ensure merge_retcodes works."""
        # It is assumed that highest retcode is the "worst".
        result = misc.RET.merge_retcodes([misc.RET.PROVISIONING_PASSED, misc.RET.PROVISIONING_FAILED])
        self.assertEqual(result, misc.RET.PROVISIONING_FAILED)

        # Empty list -> RET.PROVISIONING_PASSED
        result = misc.RET.merge_retcodes([])
        self.assertEqual(result, misc.RET.PROVISIONING_PASSED)

    def test_is_task_waived(self):
        """Ensure is_task_waived works."""
        self.assertFalse(misc.is_task_waived('invalid'))

        # Test invalid parameter
        self.assertFalse(misc.is_task_waived(BS('<task name="a4"><params><param/></params></task>', 'xml')))

        # Test with CKI_WAIVED being true
        self.assertTrue(misc.is_task_waived(BS('<task name="a4"><params><param name="CKI_WAIVED" value="true" />'
                                               '</params></task>', 'xml')))

        # Test with CKI_WAIVED being False
        self.assertFalse(misc.is_task_waived(BS('<task name="a4"><params><param name="CKI_WAIVED" value="False" />'
                                                '</params></task>', 'xml')))

        # Test with CKI_WAIVED being true
        self.assertFalse(misc.is_task_waived(BS('<task name="a4"/>', 'xml')))

    def test_monotonic(self):
        """Ensure monotonic.get() works."""
        mono = misc.Monotonic()
        self.assertEqual(mono.get(), 1)
        self.assertEqual(mono.get(), 2)

    @mock.patch('upt.misc.LOGGER.debug')
    @mock.patch('upt.misc.SESSION')
    @mock.patch('upt.misc.File')
    def test_sanitize(self, mock_file, mock_session, mock_debug):
        # pylint: disable=no-self-use
        """Ensure sanitize works."""
        mock_path = mock.Mock()
        slink = misc.SanitizeLink('url1', mock_path, kernel_version='3.18')

        slink.save()

        mock_debug.assert_called_with('Downloading %s', 'url1')
        mock_session.get.assert_called_with('url1')
        mock_file.return_value.sanitize.assert_called()

    def test_ret_to_yaml(self):
        """Ensure RET serializes correctly."""
        result = misc.RET.to_yaml(RoundTripRepresenter(), misc.RET.PROVISIONING_FAILED)
        self.assertIsInstance(result, ScalarNode)
        # Both node and RET have value attributes, but are different objects.
        self.assertEqual(misc.RET.PROVISIONING_FAILED.name, result.value)
