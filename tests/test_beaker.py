"""Test cases for Beaker provisioner module."""
import copy
import itertools
import os
import pathlib
import subprocess
import unittest
from unittest import mock

from bs4 import BeautifulSoup as BS

from plumbing.end_conditions import ProvEndConds
from plumbing.format import ProvisionData
from plumbing.objects import ResourceGroup
from provisioners.beaker import Beaker
from tests.const import ASSETS_DIR
from upt.legacy import convert_xml
from upt.logger import COLORS
from upt.misc import RET


class TestBeaker(unittest.TestCase):
    # pylint: disable=too-many-public-methods,protected-access
    """Test cases for Beaker module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.xmlpath = pathlib.Path(ASSETS_DIR, 'beaker_xml')
        self.xml = self.xmlpath.read_text()

        self.noop = lambda x: x
        self.noop_reprovision = lambda: None

    def test_exclude_hosts(self):
        """Ensure exclude_hosts works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        prov_data.get_provisioner('beaker').exclude_hosts(['hostname1', 'hostname2'])

        self.assertIn('<hostname op="!=" value="hostname1"/>',
                      prov_data.get_provisioner('beaker').rgs[0].recipeset.hosts[0].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"/>',
                      prov_data.get_provisioner('beaker').rgs[0].recipeset.hosts[0].recipe_fill)

    def test_exclude_hosts_and(self):
        """Ensure exclude_hosts works with and element."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        prov_data.get_provisioner('beaker').exclude_hosts(['hostname1', 'hostname2'])

        recipeset0 = prov_data.get_provisioner('beaker').rgs[0].recipeset

        self.assertIn('<hostname op="!=" value="hostname1"/>', recipeset0.hosts[0].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"/>', recipeset0.hosts[0].recipe_fill)

        # Ensure the hostRequires element isn't changed when hostRequires force= is used.
        self.assertEqual('<hostRequires force="def"/>',
                         str(BS(recipeset0.hosts[1].recipe_fill, 'xml').find('hostRequires')))

        self.assertIn('<hostname op="!=" value="hostname1"/>', recipeset0.hosts[2].recipe_fill)
        self.assertIn('<hostname op="!=" value="hostname2"/>', recipeset0.hosts[2].recipe_fill)

        # Ensure hostname exclude isn't appended twice.
        expect = BS('<recipe ks_meta="re"> <hostRequires><and><hostname op="!=" value="hostname2" />'
                    '<hostname op="!=" value="hostname1" /></and></hostRequires></recipe>',
                    'xml').find('recipe')
        self.assertEqual(expect.prettify(), prov_data.get_provisioner('beaker').rgs[0].recipeset.hosts[2].recipe_fill)

    @mock.patch('provisioners.beaker.Beaker.release_resources', mock.Mock())
    @mock.patch('time.sleep')
    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('plumbing.end_conditions.ProvEndConds.update_provisioning_state', mock.Mock())
    @mock.patch('upt.logger.LOGGER.printc', mock.Mock())
    def test_wait(self, mock_sleep):
        """Ensure wait works."""
        bkr = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        def succeed_and_fail_intermittently(self, force_recheck, **kwargs):
            data = [False, True, False, False, True]
            succeed_and_fail_intermittently.i += 1
            return data[succeed_and_fail_intermittently.i]

        succeed_and_fail_intermittently.i = -1
        rg_not_done = ResourceGroup()
        rg_not_done._provisioning_done = False
        with mock.patch('plumbing.end_conditions.ProvEndConds.evaluate', succeed_and_fail_intermittently):
            cond = ProvEndConds(rg_not_done, lambda x: False, self.noop_reprovision, self.noop,
                                self.noop, 600)

            result = bkr.rgs[0].wait(cond, lambda x: None, lambda x: None,
                                     **{'max_aborted_count': 3})
            self.assertEqual(result, RET.PROVISIONING_FAILED)

            mock_sleep.assert_called()

    @mock.patch('provisioners.beaker.Beaker.release_resources')
    @mock.patch('time.sleep')
    @mock.patch('builtins.print')
    @mock.patch('plumbing.end_conditions.ProvEndConds.update_provisioning_state')
    def test_wait_retcode(self, mock_update, mock_print, mock_sleep, mock_release_resources):
        # pylint: disable=protected-access
        """Ensure wait works and passess retcode around."""
        bkr = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        def fake_evaluate(self, force_recheck, **kwargs):
            self.retcode = kwargs.get('desired_retcode')
            return True

        rg_not_done = ResourceGroup()
        rg_not_done._provisioning_done = False

        with mock.patch('plumbing.end_conditions.ProvEndConds.evaluate', fake_evaluate):
            cond = ProvEndConds(rg_not_done, lambda x: False, self.noop_reprovision, self.noop,
                                self.noop, 600)

            result = bkr.rgs[0].wait(cond, lambda x: None, lambda x: None,
                                     **{'max_aborted_count': 3,
                                        'desired_retcode': RET.PROVISIONING_FAILED})
            self.assertEqual(result, RET.PROVISIONING_FAILED)

            result = bkr.rgs[0].wait(cond, lambda x: None, lambda x: None,
                                     **{'max_aborted_count': 3,
                                        'desired_retcode': RET.PROVISIONING_PASSED})
            self.assertEqual(result, RET.PROVISIONING_PASSED)

    def test_get_resource_ids(self):
        """Ensure get_resource_ids works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)

        self.assertEqual(prov_data.get_provisioner('beaker').get_resource_ids(), ['J:1234'])

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('upt.logger.LOGGER.error')
    @mock.patch('upt.logger.LOGGER.info')
    def test_set_reservation_duration(self, mock_info, mock_error, mock_safe_popen):
        """Ensure set_reservation_duration works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        mock_safe_popen.return_value = ('some output', 'some error', 0)

        beaker.set_reservation_duration(beaker.rgs[0].recipeset.hosts[0])
        mock_error.assert_called_with('💀 set_reservation_duration: %s', 'some error')
        mock_info.assert_called_with('set_reservation_duration: %s', 'some output')

        mock_safe_popen.return_value = ('', '', 1)
        beaker.set_reservation_duration(beaker.rgs[0].recipeset.hosts[0])
        mock_error.assert_called_with('💀 set_reservation_duration: cannot adjust watchdog for: R:%s',
                                      beaker.rgs[0].recipeset.hosts[0].recipe_id)

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('upt.logger.LOGGER.info')
    def test_set_reservation_duration_no_stdout(self, mock_info, mock_safe_popen):
        """Ensure set_reservation_duration doesn't print stdout if there's none."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        mock_safe_popen.return_value = ('', '', 0)
        beaker.set_reservation_duration(beaker.rgs[0].recipeset.hosts[0])
        mock_info.assert_not_called()

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    @mock.patch('time.sleep', lambda *args: None)
    @mock.patch('logging.warning', mock.Mock())
    def test_get_bkr_results(self, mock_print, mock_safe_popen):
        """Ensure get_bkr_results works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        mock_safe_popen.return_value = ('some output', '', 1)
        with self.assertRaises(RuntimeError):
            beaker.get_bkr_results('1234')
            mock_print.assert_called_with('some output')

        mock_safe_popen.return_value = ('', '', 0)
        beaker.get_bkr_results('1234')

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('provisioners.beaker.Beaker.find_host_object', mock.Mock())
    def test_get_dead_resource_info_ewd(self, mock_debug):
        """Ensure get_dead_resource_info works for EWD check."""
        soup = BS(self.xml, 'xml')
        result_recipesets, recipe_ids_dead = soup.findAll('recipeSet'), set()
        result_recipes = list(itertools.chain(*[recipeset.findAll('recipe') for recipeset in
                                                result_recipesets]))

        Beaker.get_dead_resource_info([mock.Mock()], result_recipes, recipe_ids_dead,
                                      Beaker.check_for_ewd)
        self.assertEqual(recipe_ids_dead, {1})
        mock_debug.assert_called_with('#%s: found EWD in reservesys task!', '1')

    @mock.patch('upt.logger.LOGGER.printc')
    @mock.patch('provisioners.beaker.Beaker.find_host_object', mock.Mock())
    def test_get_dead_resource_info_panic(self, mock_printc):
        """Ensure get_dead_resource_info works for panic check."""
        soup = BS(self.xml, 'xml')
        result_recipesets, recipe_ids_dead = soup.findAll('recipeSet'), set()
        result_recipes = list(itertools.chain(*[recipeset.findAll('recipe') for recipeset in
                                                result_recipesets]))

        Beaker.get_dead_resource_info([mock.Mock()], result_recipes, recipe_ids_dead,
                                      Beaker.check_for_panic)
        mock_printc.assert_called_with('#1: KERNEL PANIC!', color=COLORS.RED)

        # Ids must be integer compatible.
        with self.assertRaises(ValueError):
            Beaker.get_dead_resource_info([mock.Mock()], [{'id': 'blah'}], [], [])

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('time.sleep')
    def test_quirk_wait(self, mock_sleep, mock_debug):
        """Ensure quirk_wait works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        with mock.patch.object(beaker, 'get_provisioning_state', lambda *args: (None, [], None)):
            beaker.quirk_wait(beaker.rgs[0])

            mock_debug.assert_called_with('* Caught Beaker quirk; waiting %d...', 60)
            mock_sleep.assert_called_with(60)

    def test_check_beaker_quirk(self):
        """Ensure check_beaker_quirk works."""
        recipe = BS('<recipe status="Aborted" result="New" />', 'xml').find('recipe')
        self.assertTrue(Beaker.check_beaker_quirk([recipe]))

        recipe = BS('<recipe status="Completed" result="New" />', 'xml').find('recipe')
        self.assertFalse(Beaker.check_beaker_quirk([recipe]))

        recipe = BS('<recipe status="Completed" result="New" ><task status="Completed" result="New" />'
                    '<task status="Aborted" result="New" /></recipe>', 'xml').find('recipe')
        self.assertTrue(Beaker.check_beaker_quirk([recipe]))

    @mock.patch('builtins.print')
    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('provisioners.beaker.Beaker.find_host_object', mock.Mock())
    def test_heartbeat_with_errors(self, mock_debug, mock_print):
        """Ensure heartbeat works."""
        # Convert xml in asssets
        kwargs = {'excluded_hosts': '', 'force_host_duration': False, 'provisioner': 'beaker'}
        beaker = convert_xml(self.xml, **kwargs).get_provisioner('beaker')
        soup = BS(self.xml, 'xml')
        # Set recipe_id for each host, which would normally be set by updating provisioning request.
        beaker.rgs[0].recipeset.hosts[0].recipe_id = 1
        beaker.rgs[0].recipeset.hosts[1].recipe_id = 2

        # Use a fake check_beaker_quirk function to pass on 2nd attempt.
        def fake_quirk(_):
            try:
                if fake_quirk.called:
                    return False
            except AttributeError:
                fake_quirk.called = True
            return True

        result_recipesets, recipe_ids_dead = soup.findAll('recipeSet'), set()
        result_recipes = list(itertools.chain(*[recipeset.findAll('recipe') for recipeset in result_recipesets]))
        with mock.patch.object(beaker, 'get_provisioning_state', lambda *args: (None, result_recipesets,
                                                                                None)):
            with mock.patch.object(beaker, 'check_beaker_quirk', fake_quirk):
                with mock.patch.object(beaker, 'quirk_wait', lambda x: (result_recipes)):
                    beaker.heartbeat(beaker.rgs[0], recipe_ids_dead)

        mock_debug.assert_called_with('#%s: found EWD in reservesys task!', '1')

    def test_is_provisioned(self):
        """Ensure is_provisioned calls get_provisioning_state that returns 3 item tuple for Beaker."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        with mock.patch.object(beaker, 'get_provisioning_state', lambda *args: (True, None, None)):
            self.assertTrue(beaker.is_provisioned([None]))

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue(self, mock_print):
        """Ensure warn_once_provisioning_issue works for recipes."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker._warned_rsetids = set()
        beaker.warn_once_provisioning_issue(1, [1, 2], 1, 'Warn', '')
        mock_print.assert_called_with('* RS:1 ([1, 2]): R:1 Warn.')

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue_reservesys(self, mock_print):
        """Ensure warn_once_provisioning_issue works for reservesys tasks."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker._warned_rsetids = set()
        beaker.warn_once_provisioning_issue(1, [1, 2], 1, None, 'Warn reservesys')
        mock_print.assert_called_with('* RS:1 ([1, 2]): found reservesys task that Warn reservesys.')

    def test_warn_once_provisioning_issue_unhandled(self):
        """Ensure warn_once_provisioning_issue checks unhandled conditions."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker._warned_rsetids = set()

        with self.assertRaises(RuntimeError):
            beaker.warn_once_provisioning_issue(0, [], 1, None, None)

    @mock.patch('builtins.print')
    def test_warn_once_provisioning_issue_no_print(self, mock_print):
        """Ensure warn_once_provisioning_issue doesn't print twice."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker._warned_rsetids = {1}
        beaker.warn_once_provisioning_issue(1, [], 1, 'err', 'err')
        mock_print.assert_not_called()

    @mock.patch('upt.logger.LOGGER.warning')
    def test_delete_failed_hosts(self, mock_warn):
        """Ensure delete_failed_hosts works and leaves only recipes with ids."""
        kwargs = {'excluded_hosts': '', 'force_host_duration': False, 'provisioner': 'beaker'}
        beaker = convert_xml(self.xml, **kwargs).get_provisioner('beaker')
        soup = BS(beaker.rgs[1].recipeset.restraint_xml, 'xml')
        soup.find('recipe')['id'] = 1

        beaker.rgs[1].recipeset.restraint_xml = soup.prettify()

        beaker.delete_failed_hosts(beaker.rgs[1].recipeset)

        self.assertEqual(len(BS(beaker.rgs[1].recipeset.restraint_xml, 'xml').findAll('recipe')), 1)
        mock_warn.assert_called_with('%s removed from XML, no fetch element!', '/distribution/reservesys')

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    @mock.patch('upt.logger.LOGGER.printc', mock.Mock())
    def test_update_provisioning_request(self):
        """Ensure update_provisioning_request works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        with mock.patch.object(beaker, 'get_bkr_results') as mock_results:
            mock_results.return_value = self.xml

            with mock.patch.object(beaker.rgs[0], 'preprovisioned', True):
                beaker.update_provisioning_request(beaker.rgs[0])

            beaker.update_provisioning_request(beaker.rgs[0])

            self.assertIn('<recipe id="1"', beaker.rgs[0].recipeset.restraint_xml)

    def test_resource_group2xml(self):
        """Ensure resource_group2xml works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        result = beaker.resource_group2xml(beaker.rgs[0])
        result = BS(result, 'xml')

        self.assertIsNotNone(result.find('job'))
        self.assertIsNotNone(result.find('recipeSet'))

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_preprovision_disabled(self):
        """Ensure disabled preprovisioning doesn't return any new xmls or resource groups."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        new_xmls, new_resource_groups = beaker.preprovision(**{'preprovision': None})
        self.assertEqual(new_xmls, new_resource_groups, [])

    def test_preprovision(self):
        """Ensure preprovision works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker.rgs.append(copy.deepcopy(beaker.rgs[0]))
        new_xmls, new_resource_groups = beaker.preprovision(**{'preprovision': '1'})

        self.assertEqual(len(new_xmls), 1)
        self.assertEqual(len(new_resource_groups), 1)

    def test_provision_preprovison(self):
        """Ensure provision works with preprovisioning."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        with mock.patch.object(beaker, 'submit_job') as mock_submit:
            mock_submit.side_effect = [['J:1234'], ['J:5678']]
            all_resource_ids = beaker.provision(**{'preprovision': '0'})

        self.assertEqual(all_resource_ids, ['J:1234', 'J:5678'])

    def test_provision(self):
        """Ensure provision works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        with mock.patch.object(beaker, 'submit_job') as mock_submit:
            mock_submit.side_effect = [['J:1234']]
            all_resource_ids = beaker.provision(**{'preprovision': None})

        self.assertEqual(all_resource_ids, ['J:1234'])

    def test_set_harness(self):
        """Ensure set_harness works."""
        soup = BS('''<recipe ks_meta="harness='restraint-rhts beakerlib'" />''', 'xml')
        Beaker.set_harness(soup)
        self.assertIn('''<recipe ks_meta="harness='restraint-rhts beakerlib'"/>''', soup.prettify())

        soup = BS('''<recipe ks_meta="" />''', 'xml')
        Beaker.set_harness(soup)
        self.assertIn('''<recipe ks_meta="harness='restraint-rhts beakerlib'"/>''', soup.prettify())

    def test_finalize_recipes(self):
        """Ensure _finalize_recipes works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        # Use any valid xml to see that  reservesys is added.
        soup = BS(beaker.rgs[0].recipeset.restraint_xml, 'xml')
        self.assertNotIn('/distribution/reservesys', soup.prettify())
        result = beaker._finalize_recipes(soup, beaker.rgs[0].recipeset)
        self.assertIn('/distribution/reservesys', result)

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_submit_job(self, mock_print, mock_safe_popen):
        """Ensure submit_job works."""
        stdout = 'J:1234 J:5678'
        mock_safe_popen.return_value = (stdout, '', 0)
        resource_ids = Beaker.submit_job('whatever')

        self.assertEqual(resource_ids, stdout.split(' '))

        mock_print.assert_called_with(f'Submitted: {" ".join(resource_ids)}')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    @mock.patch('upt.logger.LOGGER.error')
    def test_submit_job_err(self, mock_error, mock_print, mock_safe_popen):
        """Ensure submit_job raises exception and prints stdout on non-zero retcode."""

        mock_safe_popen.return_value = ('stdout', '', 1)

        Beaker.submit_job('whatever')

        mock_print.assert_called_with('stdout')
        mock_error.assert_called_with('submitting job failed!')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('provisioners.beaker.sys.stderr.write')
    def test_submit_job_supress(self, mock_write, mock_safe_popen):
        """Ensure submit_job raises exception and prints stdout on non-zero retcode."""

        mock_safe_popen.return_value = ('',  'somefail\nWARNING: job xml validation failed: '
                                             'Element task has extra content', 0)
        Beaker.submit_job('whatever')

        mock_write.assert_called_with('somefail\n')

    @mock.patch('cki_lib.misc.safe_popen')
    def test_clone(self, mock_safe_popen):
        """Ensure clone works."""
        stdout = 'J:1234 J:5678'
        mock_safe_popen.return_value = (stdout, '', 0)
        resource_ids = Beaker.clone('whatever')

        self.assertEqual(resource_ids, stdout.split(' '))

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_clone_err(self, mock_print, mock_safe_popen):
        """Ensure clone raises exception and prints stdout on non-zero retcode."""

        mock_safe_popen.return_value = ('stdout', '', 1)
        with self.assertRaises(RuntimeError):
            Beaker.clone('whatever')

        mock_print.assert_called_with('stdout')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    def test_cancel(self, mock_print, mock_safe_popen):
        # pylint: disable=no-self-use
        """Ensure cancel works."""

        mock_safe_popen.return_value = ('stdout', '', 0)
        Beaker.cancel('whatever')

        mock_print.assert_called_with('stdout')

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('builtins.print')
    @mock.patch('upt.logger.LOGGER.printc')
    def test_cancel_raise_on_error(self, mock_printc, mock_print, mock_safe_popen):
        """Ensure cancel works."""

        mock_safe_popen.return_value = ('stdout', '', 1)
        Beaker.cancel('whatever')
        mock_printc.assert_called_with('Failed to release resources(s)', color=COLORS.RED)
        mock_print.assert_called_with('stdout')

    @mock.patch('builtins.print')
    def test_reprovision_aborted(self, mock_print):
        """Ensure reprovision_aborted works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        beaker.rgs[0]._erred_rset_ids = {1}

        with mock.patch.object(beaker, 'cancel'):
            with mock.patch.object(beaker, 'get_bkr_results') as fake_results:
                fake_results.return_value = pathlib.Path(self.req_asset).read_text()
                with mock.patch.object(beaker, 'clone') as fake_clone:
                    fake_clone.return_value = ['J:1234']

                    beaker.reprovision_aborted()
                    mock_print.assert_called_with('* Cloned RS:1 as J:1234')

    def test_release_resources(self):
        """Ensure release_resources works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        with mock.patch.object(beaker, 'cancel') as mock_cancel:
            beaker.release_resources()
            mock_cancel.assert_called_with('J:1234')

    @mock.patch('builtins.print')
    def test_get_provisioning_state(self, mock_print):
        """Ensure get_provisioning_state works."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        with mock.patch.object(beaker, 'get_bkr_results') as fake_results:
            fake_results.return_value = self.xml
            provisioned, recipesets, erred_rset_ids = beaker.get_provisioning_state(beaker.rgs[0])
            self.assertFalse(provisioned)
            self.assertEqual(len(recipesets), 3)
            self.assertEqual(erred_rset_ids, {2, 3})

            mock_print.assert_any_call("* RS:2 (['2', '3', '4', '0']): R:0 Aborted.")
            mock_print.assert_any_call("* RS:3 (['6']): R:6 Aborted.")

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.logger.LOGGER.warning')
    @mock.patch('upt.logger.LOGGER.printc')
    def test_update_provisioning_request_inconsistency(self, mock_printc, mock_warning):
        """Ensure update_provisioning_request works, test for inconsistency of new bkr-results and internal data."""
        beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')

        with mock.patch.object(beaker, 'get_bkr_results') as fake_results:
            fake_results.return_value = self.xml
            beaker.rgs[0]._erred_rset_ids = {1}
            beaker.update_provisioning_request(beaker.rgs[0])
            assert mock_warning.call_count == 4

            beaker.rgs[0]._erred_rset_ids = set()
            beaker.update_provisioning_request(beaker.rgs[0])
            mock_printc.assert_any_call('Error: a system went down shortly after provisioning '
                                        'was completed!', color=COLORS.RED)

    @mock.patch('cki_lib.misc.safe_popen')
    @mock.patch('upt.logger.LOGGER.error')
    def test_get_links2logs_fail(self, mock_error, mock_safe_popen):
        """Ensure get_links2logs reports non-zero retcode."""
        mock_safe_popen.return_value = ('', '', 1)

        Beaker.get_links2logs(1234)

        mock_error.assert_called_with('bkr job-logs issue')

    @mock.patch('cki_lib.misc.safe_popen')
    def test_get_links2logs(self, mock_safe_popen):
        """Ensure get_links2logs works."""
        base_url = 'https://beaker.engineering.redhat.com/recipes/'
        will_be_in_result = [f'{base_url}/1234/logs/console.log']
        wont_be_in_result = [f'{base_url}/9999/logs/console.log/',
                             f'{base_url}/1234/logs/sys.log']
        mock_safe_popen.return_value = ('\n'.join(will_be_in_result + wont_be_in_result), '', 0)

        result = Beaker.get_links2logs(1234)

        mock_safe_popen.assert_called_with(['bkr', 'job-logs', 'R:1234'], stdout=subprocess.PIPE)
        self.assertEqual(will_be_in_result, result)
