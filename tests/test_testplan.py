"""Test cases for plan_of_tests module."""
import os
import unittest
from unittest import mock

from bs4 import BeautifulSoup as BS

from plumbing.format import ProvisionData
from plumbing.objects import Host
from plumbing.objects import ResourceGroup
from restraint_wrap.task_result import TaskResult
from restraint_wrap.testplan import TestPlan
from tests.const import ASSETS_DIR
from upt.misc import OutputDirCounter


class TestTestPlan(unittest.TestCase):
    """Test cases for TestPlan module."""

    def setUp(self) -> None:
        mock1 = mock.patch('restraint_wrap.testplan.LOGGER.printc', mock.Mock())
        mock1.start()

        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.beaker = self.provision_data.get_provisioner('beaker')

        self.testplan = TestPlan(self.provision_data.provisioners,
                                 **{'output': 'whatever-dir'})

    @mock.patch('restraint_wrap.testplan.RestraintStandaloneTest', mock.Mock())
    @mock.patch('restraint_wrap.kcidb_adapter.KCIDBTestAdapter.dump')
    def test_dump_testplan(self, mock_dump):
        """Ensure dump_testplan works."""
        # NOTE: RestraintStandaloneTest is mocked and tested elsewhere, so this test is brief.

        self.testplan.dump_testplan()
        mock_dump.assert_called()

    @mock.patch('restraint_wrap.testplan.RestraintStandaloneTest', mock.Mock())
    @mock.patch('restraint_wrap.kcidb_adapter.KCIDBTestAdapter.dump')
    @mock.patch('restraint_wrap.testplan.pathlib.Path.mkdir', lambda *args, **kwargs: None)
    def test_on_task_result(self, mock_dump_test):
        """Ensure on_task_result works."""
        resource_group = ResourceGroup()
        task_content = '<task id="1" name="a3" status="Completed" result="Pass"> <fetch url="git://"/></task>'
        tasks = [BS(task_content, 'xml').find('task')]
        host = Host({'hostname': 'hostname1', 'recipe_id': 1234})
        host._counter = OutputDirCounter()
        task_result = TaskResult(host, tasks[0], 1, 'New', 'Running')

        self.testplan.on_task_result(self.beaker.rgs[0], task_result, 'job.01', 'w.json')
        mock_dump_test.assert_not_called()

        task_result = TaskResult(host, tasks[0], 1, 'Fail', 'Aborted')
        self.testplan.on_task_result(resource_group, task_result, 'job.01', 'w.json')
        mock_dump_test.assert_called()

    def test_create_testplan(self):
        # pylint: disable=protected-access
        """Ensure create_testplan populates _planned_tests correctly."""
        recipeset = self.beaker.rgs[0].recipeset
        self.testplan.create_testplan('whatever-dir', recipeset)

        target_host1 = recipeset.hosts[1]
        target_host2 = recipeset.hosts[2]

        # The recipe_id checks are to check the consistency of code and assets test data.
        self.assertEqual(target_host1.recipe_id, 456)
        self.assertEqual(target_host1._planned_tests[0].testname, "TestUsedToCheckTestPlanLogic1")
        self.assertEqual(target_host2.recipe_id, 789)
        self.assertEqual(target_host2._planned_tests[0].testname, "TestUsedToCheckTestPlanLogic2")

    @mock.patch('restraint_wrap.testplan.BS')
    @mock.patch('restraint_wrap.testplan.TaskResult')
    @mock.patch('upt.logger.LOGGER.debug')
    def test_create_testplan_no_dump_setup(self, mock_debug, mock_task_result, mock_bs):
        """Ensure that testplan doesn't dump setup tasks."""
        mock_rs = mock.Mock()
        host = mock.Mock()
        host._planned_tests = [None, None]
        mock_task_result.return_value.cki_name = None
        mock_task_result.return_value.universal_id = None
        mock_task_result.return_value.testname = 'setup'
        mock_rs.hosts = [host]
        mock_bs.return_value.findAll.return_value = ['a', 'b']
        mock_bs.return_value.find.return_value = mock_bs.return_value

        TestPlan.create_testplan('whatever-dir', mock_rs)
        mock_debug.assert_called_with('%s is a setup task - not part of testplan.', 'setup')
