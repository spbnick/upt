"""Test cases for glue module."""
import itertools
import os
from queue import Empty
import sys
import unittest
from unittest import mock

from cki_lib.misc import tempfile_from_string

from plumbing.format import ProvisionData
from plumbing.objects import ResourceGroup
from restraint_wrap.misc import RESTRET
from tests.const import ASSETS_DIR
from upt.glue import ProvisionerGlue
from upt.misc import RET


class TestGlue(unittest.TestCase):
    """Test cases for glue module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')
        self.prov_data = ProvisionData.deserialize_file(self.req_asset)
        self.mock_rc_data = mock.Mock()
        self.mock_prov = mock.Mock()
        self.resource_group = mock.Mock()
        self.resource_group.recipeset.hosts = ['something']
        self.mock_prov.rgs = [self.resource_group]
        self.mock_data = mock.Mock()
        self.mock_data.provisioners = [self.mock_prov]

    @mock.patch('plumbing.objects.ResourceGroup.wait', new=lambda *a, **k: RET.PROVISIONING_PASSED)
    @mock.patch('upt.glue.SimpleQueue')
    def test_do_wait(self, mock_queue):
        """Ensure do_wait calls expected methods."""
        bkr = self.prov_data.get_provisioner('beaker')
        bkr.rgs[0].status = RET.PROVISIONING_PASSED
        with mock.patch.object(bkr, 'update_provisioning_request', new=lambda rg: None):
            with tempfile_from_string(b'') as fname:
                mock_rc = mock.Mock()
                glue = ProvisionerGlue(self.prov_data, **{'request_file': fname,
                                                          'rc': 'whatever-path',
                                                          'pipeline': False,
                                                          'rc_data': None})

                mock_queue.return_value.get.return_value = (0, 0, RET.PROVISIONING_PASSED,
                                                            bkr.rgs[0])
                glue.do_wait()

                glue = ProvisionerGlue(self.prov_data, **{'request_file': fname,
                                                          'rc': 'whatever-path',
                                                          'pipeline': False,
                                                          'rc_data': mock_rc})
                self.assertEqual(glue.do_wait(), RET.PROVISIONING_PASSED)
                glue = ProvisionerGlue(self.prov_data, **{'request_file': fname,
                                                          'rc': 'whatever-path',
                                                          'pipeline': False,
                                                          'rc_data': mock_rc})
                self.assertEqual(glue.do_wait(), RET.PROVISIONING_PASSED)

    @mock.patch('upt.glue.ProvisionerGlue.do_wait', return_value=RET.PROVISIONING_FAILED)
    @mock.patch('plumbing.format.ProvisionData.serialize2file', mock.Mock())
    @mock.patch('provisioners.beaker.Beaker.provision', mock.Mock())
    @mock.patch('provisioners.beaker.Beaker.update_provisioning_request', mock.Mock())
    @mock.patch('upt.glue.reactor', mock.Mock())
    def test_run_provisioners(self, _):
        """Ensure run_provisioners works and calls expected methods."""
        with tempfile_from_string(b'') as fname:
            glue = ProvisionerGlue(self.mock_data, **{'request_file': fname, 'rc': 'whatever-path',
                                                      'rc_data': self.mock_rc_data})
            self.assertEqual(RET.PROVISIONING_FAILED, glue.run_provisioners())

    @mock.patch('upt.glue.SimpleQueue')
    def test_update_provisioners(self, mock_queue):
        """Ensure update_provisioners works."""
        glue = ProvisionerGlue(self.mock_data, **{'request_file': 'ignore', 'rc': 'whatever-path',
                                                  'rc_data': self.mock_rc_data, 'pipeline': 'arg'})

        mock_queue.return_value.get.return_value = (0, 0, 0, self.resource_group)
        glue.update_provisioners(1)
        self.assertEqual(RESTRET.KERNEL_TESTING_PASSED, glue.retcode)

        mock_queue.return_value.get.side_effect = \
            itertools.chain([Empty()], itertools.cycle([(0, 0, 0, self.resource_group)]))
        glue.update_provisioners(1)

    @mock.patch('upt.glue.ProvisionData', mock.Mock())
    @mock.patch('upt.glue.safe_popen', return_value=('', '', 0))
    @mock.patch('upt.glue.SimpleQueue', mock.Mock())
    def test_process_resource_group(self, mock_popen):
        """Ensure process_resource_group works."""
        kwargs = {'request_file': 'ignore', 'rc': 'whatever-path',
                  'rc_data': self.mock_rc_data, 'pipeline': 'arg1'}
        glue = ProvisionerGlue(self.mock_data, **kwargs)

        glue.process_resource_group(self.mock_prov, self.resource_group, 0, 0, 0, **kwargs)
        mock_popen.assert_called()

    @mock.patch('upt.glue.ProvisionData', mock.Mock())
    @mock.patch('upt.glue.safe_popen', return_value=('', '', 0))
    def test_invoke_restraint_runner(self, mock_popen):
        """Ensure invoke_restraint_runner works."""
        kwargs = {'request_file': 'ignore', 'rc': 'whatever-path', 'rc_data': self.mock_rc_data,
                  'pipeline': 'arg1'}
        glue = ProvisionerGlue(self.mock_data, **kwargs)
        resource_group = ResourceGroup()
        resource_group.recipeset.hosts = []
        result = glue.invoke_restraint_runner(resource_group, 0, 1)
        self.assertEqual(result, 1)

        sys_arv_cpy = sys.argv.copy() + ['--pipeline', 'arg1', '-i', 'whatever']
        with mock.patch.object(sys, 'argv', sys_arv_cpy):
            resource_group.recipeset.hosts = ['something']
            result = glue.invoke_restraint_runner(resource_group, 0, 1)
            self.assertEqual(result, 0)
            mock_popen.assert_called()
