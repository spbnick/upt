#!/usr/bin/env python3
"""Main entry point, command-line interface for upt."""
import os
import pathlib
import sys

from cki_lib.logger import logger_add_fhandler
from cki_lib.logger import truncate_logs
from cki_lib.session import get_session
import click
from rcdefinition.rc_data import SKTData

from plumbing.aws_setup import AWSSetup
from plumbing.format import ProvisionData
from upt.glue import ProvisionerGlue
from upt.legacy import legacy
from upt.logger import LOGGER

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)
@click.option('--time-cap', required=False, default=0, type=int,
              help='Specifies a time limit in minutes to finish provisioning. If the'
                   ' provisioner fails to provision within this time limit,'
                   ' then all resource(s) will be returned (e.g. Beaker job is'
                   ' canceled). Default: 0 (disabled).')
@click.option('--max-aborted-count', required=False, default=3, type=int,
              help='Specifies the number of attempts to provision a resource.'
                   'Default: 3.')
@click.option('--force-host-duration', required=False, default=None, type=int,
              help='If used, forces a static duration for how long the '
                   'resource is provisioned. If not used, we add-up '
                   'KILLTIMEOVERRIDE values of tasks in each recipe.')
@click.option('--preprovision', default='', type=str,
              help='Comma separated list of indexes of resource groups in provisioning request that are to be copied '
                   '(preprovisioned) and left on standby for re-running tests. Tests may be arbitrarily spread among '
                   'the hosts. Default: "" (not used).')
@click.option('--rc', default=os.environ.get('RC_FILE', 'rc'), help='Path to rc file. Defaults to RC_FILE env. '
                                                                    'variable or "rc". If it does not exist, '
                                                                    'data is not written to rc file and '
                                                                    'console logs will include distro kernel boot'
                                                                    ' log.')
@click.option('-e', '--excluded-hosts', required=False, default='',
              help='A path to a file with newline separated hostnames that we do not want to run tests on.')
@click.option('--keycheck/--no-keycheck', default=False, help='Do strict ssh host keychecking. Default: False.')
@click.option('--log-dir', default=os.environ.get('UPT_OUTPUT_DIR', os.getcwd()),
              help='Output directory for the logfile. Default: specified by UPT_OUTPUT_DIR environment variable, '
                   'otherwise current directory.')
@click.pass_context
def cli(ctx, **kwargs):
    """Click cli entry point."""
    # ensure that ctx.obj exists and is a dict (in case `cli()` is called
    # by means other than the `if` block below
    print('UPT CLI started')
    ctx.ensure_object(dict)

    # Put log file info.log into current directory.
    logger_add_fhandler(LOGGER, 'upt_info.log', kwargs.get('log_dir'))

    kwargs['keycheck'] = 'yes' if kwargs['keycheck'] else 'no'
    ctx.params['legacy'] = False

    # Open rc-file, deserialize it
    path2rc = pathlib.Path(kwargs['rc'])
    ctx.params['rc_data'] = SKTData.deserialize(path2rc.read_text(encoding='utf8')) \
        if path2rc.is_file() else None


@cli.command()
@click.option('-r', '--request-file', required=True, default='req.yaml',
              help='Path to provisioner data. Default: req.yaml.', )
@click.option('--pipeline', required=False, default='',
              help='Run provisioning and testing asynchronously, if used. Specifies params to '
                   'pass to test runner.')
@click.pass_context
def provision(ctx, **kwargs):
    """Provision resources according to --request_file."""
    print('Provisioning resources...')
    kwargs.update(ctx.parent.params)

    provisioner_data = ProvisionData.deserialize_file(kwargs['request_file'])
    glue = ProvisionerGlue(provisioner_data, **kwargs)
    retcode = glue.run_provisioners()
    sys.exit(retcode.value)


@cli.command()
@click.option('-r', '--request-file', required=True, default='c_req.yaml',
              help='Path to provisioner data. Default: c_req.yaml.', )
@click.pass_context
def cancel(ctx, **kwargs):
    """Release all resources in --request-file."""
    LOGGER.debug('Returning all resources back to provisioner(s).')
    kwargs.update(ctx.parent.params)

    provisioner_data = ProvisionData.deserialize_file(kwargs['request_file'])
    for prov in provisioner_data.provisioners:
        prov.release_resources()


@cli.command()
@click.option('-k', '--key-out', help='A path to a filename to create a new ssh key if it does '
                                      'not already exist in the account.', )
@click.option('-i', '--instance-type', required=True, default='t2.micro',
              help='Instance type to use. Default: t2.micro')
@click.option('--ip', default=None,
              help='Specify an IP to let through firewall to your instance. Example: 1.2.3.4. '
                   'Default: (none, will use your IP.)')
@click.pass_context
def aws_setup(ctx, **kwargs):
    """Create a launch template in user's AWS account."""
    print('Creating launch template for UPT in your AWS account...')
    kwargs.update(ctx.parent.params)

    your_ip = kwargs.get('ip')
    if not your_ip:
        session = get_session(__name__)
        your_ip = session.get('https://api.ipify.org').text

    # Setup aws account, create a launch template.
    aws_helper = AWSSetup()
    aws_helper.setup(your_ip + '/32', kwargs.get('key_out'), kwargs.get('instance_type'))


@cli.command()
@click.option('-r', '--request-file', required=True, default='c_req.yaml',
              help='Path to provisioning request. Default: c_req.yaml.')
@click.pass_context
def waiton(ctx, **kwargs):
    # pylint: disable=too-many-arguments
    """Wait on already submitted Beaker jobid matching provisioning request."""
    # pack all arguments into kwargs
    kwargs.update(ctx.parent.params)
    print('Waiting on existing provisioning request...')

    # load provisioning request, this has to match the job that was submitted
    provisioner_data = ProvisionData.deserialize_file(kwargs['request_file'])
    glue = ProvisionerGlue(provisioner_data, **kwargs)

    # create helper object
    # wait on Beaker job
    glue.do_wait()


cli.add_command(legacy)


def main():
    """Do everything; main entrypoint."""
    # pylint: disable=no-value-for-parameter
    truncate_logs()
    cli()


if __name__ == '__main__':
    main()
