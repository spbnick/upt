"""A plan of tests to run."""
from itertools import chain
import os
import pathlib

from bs4 import BeautifulSoup as BS
from kcidb_wrap.v4 import craft_kcidb_data

from plumbing.interface import ProvisionerCore
from plumbing.objects import ResourceGroup
from restraint_wrap.kcidb_adapter import KCIDBTestAdapter
from restraint_wrap.kcidb_adapter import RestraintStandaloneTest
from restraint_wrap.misc import convert_path_to_link
from restraint_wrap.task_result import TaskResult
from upt.logger import LOGGER
from upt.misc import OutputDirCounter


class TestPlan:
    """A plan of tests to run."""

    # Prevent pytest from trying to collect TestPlan as tests
    __test__ = False

    def __init__(self, provisioners, **kwargs):
        """Create the object."""
        # Command-line arguments.
        self.kwargs = kwargs

        # Global output directory.
        self.output = kwargs.get('output')

        # Provisioner objects we got from yaml.
        self.provisioners = provisioners

        # Optional kcidb adapter when we want to dump kcidb data.
        self.adapter = KCIDBTestAdapter(**kwargs) if kwargs.get('dump') else None

        # Go through all non-preprovisioned resource groups and assigned counter to each host.
        for recipeset in chain(*[ProvisionerCore.find_objects(chain(*[prov.rgs for prov in self.provisioners]),
                                                              lambda rg: rg.recipeset if
                                                              isinstance(rg, ResourceGroup) and not
                                                              rg.preprovisioned else None)]):
            self.create_testplan(self.output, recipeset)

    @classmethod
    def create_testplan(cls, output, recipeset):
        # pylint: disable=protected-access
        """Populate _planned_tests in hosts in this recipeset with TaskResult objects that match tests to run."""
        # Restraint xml contains "raw", _complete_ specification of tasks to run.
        soup = BS(recipeset.restraint_xml, 'xml')
        basepath = pathlib.Path(output).parent

        # Process everything in this recipeset.
        for host in recipeset.hosts:
            # NOTE: empty _planned_tests first in case we ran this twice!
            host._planned_tests = []

            # We build testplan based on input data and adhere to it. The _counter is initially
            # assigned only here. Any code be adjusting the number of hosts in testplan should go
            # through TestPlan class.
            host._counter = OutputDirCounter()
            path2host = pathlib.Path(output, host._counter.path)
            LOGGER.printc(f'Results for {host.hostname} will be present in the job artifacts in: '
                          f'{path2host.relative_to(basepath)}')

            link = convert_path_to_link(path2host.joinpath('index.html').relative_to(basepath),
                                        True)
            LOGGER.printc(f'Please see the index.html file for results when the job finishes: '
                          f'{link}')

            recipe = soup.find('recipe', {'id': host.recipe_id})
            assert recipe is not None, "Invalid data or broken code. Cannot find recipe_id specified."
            tasks_of_this_host = recipe.findAll('task')
            i_for_task_id = 0
            for task in tasks_of_this_host:
                prev_task = host._planned_tests[i_for_task_id - 1] if i_for_task_id > 0 else None

                # First task_id must be 1.
                task_result = TaskResult(host, task, i_for_task_id + 1, None, None, prev_task=prev_task)
                task_result.start_time = None
                if not task_result.cki_name and not task_result.universal_id:
                    LOGGER.debug('%s is a setup task - not part of testplan.',
                                 task_result.testname)
                    # Don't increase i_for_task_id.
                    continue

                # Append a placeholder object into _planned_tests attribute.
                host._planned_tests.append(task_result)

                i_for_task_id += 1

    def on_task_result(self, resource_group, task_result, run_suffix, fname):
        # pylint: disable=protected-access
        """Create a kcidb Test object when a result is available.

        Arguments:
            resource_group: resource group object from provisioning request to identify origin of this test
            task_result:    TaskResult object that contains result of a test that finished.
            run_suffix:     str, subdirectory created by restraint client, e.g './job.01'
            fname:          str, a name of the file to write results to
        """
        # Check if task is finished
        if task_result.status in ['Aborted', 'Cancelled', 'Completed']:
            LOGGER.debug('Task R:%i T:%i is done and being dumped.', task_result.recipe_id, task_result.task_id)

            original_taskid = task_result.task_id if resource_group.recipeset._attempts_made == 0 \
                else task_result.host._rerun_recipe_tasks_from_index + task_result.task_id - 1

            # We create a kcidb Test object when the test is finished. This is considered to be incremental result,
            # because there may be other tests queued for this host & kernel. We don't include state transitions
            # of the tests in the results.
            new_test = RestraintStandaloneTest(self.adapter, task_result,
                                               original_task_id=original_taskid,
                                               run_suffix=run_suffix)

            kcidb_data = craft_kcidb_data([], [], [new_test.render()])

            # Dump data to global output directory /{counter_number:04d}/
            fpath_out = pathlib.Path(self.output, task_result.host._counter.path)
            # Ensure directory exists
            fpath_out.mkdir(exist_ok=True, parents=True)
            KCIDBTestAdapter.dump(kcidb_data, str(fpath_out.joinpath(fname)))

    def dump_testplan(self):
        # pylint: disable=protected-access
        """Dump planned tests of all non-preprovisioned resource_groups into a single kcidb-data json file."""
        tests2dump = []

        for host in chain(*ProvisionerCore.find_objects(chain(*[prov.rgs for prov in self.provisioners]),
                                                        lambda rg: rg.recipeset.hosts if
                                                        isinstance(rg, ResourceGroup) and not
                                                        rg.preprovisioned else None)):
            for task_result in host._planned_tests:
                # run_suffix is None -> Don't try to get files that tests created, the tests didn't even run!
                tests2dump.append(RestraintStandaloneTest(self.adapter, task_result, testplan=True).render())

        kcidb_data = craft_kcidb_data([], [], tests2dump)

        # Dump testplan to global output directory.
        KCIDBTestAdapter.dump(kcidb_data, os.path.join(self.output, 'kcidb_testplan.json'))
