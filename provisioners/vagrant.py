"""Vagrant provisioner/provider."""
import re
import subprocess

from cki_lib.misc import safe_popen

from plumbing.interface import LimitedSynchronous
from upt.logger import LOGGER


class Vagrant(LimitedSynchronous):
    """Vagrant provisioner."""

    def provision(self, **kwargs):
        """Provision all hosts in all resource groups."""
        self._provision(**kwargs)

    def provision_host(self, host):
        """Provision a single host."""
        stdout, stderr, _ = safe_popen(['vagrant', 'global-status'], stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
        if stderr:
            LOGGER.error(stderr)

        rgx_status = re.compile(r'([0-9a-f]+)\s+(.*?)\s+(.*?)\s+(.*?)\s+([^\n\s]+)')
        for line in stdout.splitlines():
            match = rgx_status.match(line)
            if match:
                temp_virt_id, directory = match.group(1), match.group(5)
                if temp_virt_id == host.misc['instance_id']:
                    stdout, _, _ = safe_popen(['vagrant', 'up'], cwd=directory,
                                              stdout=subprocess.PIPE)
                    LOGGER.debug(stdout)

                    stdout, _, _ = safe_popen(['vagrant', 'ssh-config'], cwd=directory,
                                              stdout=subprocess.PIPE)
                    rgx_match = re.finditer('HostName ([^\n]+)', stdout, re.IGNORECASE)
                    host.hostname = next(rgx_match).group(1)

                    # NOTE: you can consider parsing 'IdentityFile ([^\n]+)' as well and setting
                    # host.ssh_opts
                    return
